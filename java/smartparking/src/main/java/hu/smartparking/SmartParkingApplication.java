package hu.smartparking;

import hu.smartparking.services.MQTTReceiverService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartParkingApplication implements CommandLineRunner {

	@Autowired
	MQTTReceiverService mqttReceiverService;

	public static void main(String[] args) {
		SpringApplication.run(SmartParkingApplication.class, args);
	}

	@Override
	public void run(String... args) {
		mqttReceiverService.startMqtt();
	}
}
