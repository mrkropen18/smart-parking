package hu.smartparking.services;

import hu.smartparking.entities.Vehicle;
import hu.smartparking.repositories.VehicleRepository;
import hu.smartparking.services.handlers.MessageHandler;
import hu.smartparking.utility.VehicleAcceptanceMsg;
import hu.smartparking.utility.VehicleMsg;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MQTTReceiverService implements MqttCallback {

    private MqttClient client = null;
    private final String mqttUrl;
    private final int garageSize;
    private final String[] topicsToSubscribe = {"ARRIVES","LEAVE","PAYMENT"};
    private final ObjectMapper mapper;

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    MQTTReceiverService(@Value("${mqtt.url}") String mqttUrl, @Value("${garageSize}") int garageSize) {
        mapper = new ObjectMapper();
        this.mqttUrl = mqttUrl;
        this.garageSize = garageSize;
        System.out.println(mqttUrl);
    }

    @Autowired
    MQTTSenderService mqttSenderService;

    @Override
    public void connectionLost(Throwable arg0) {
        System.out.println("[LOG] MQTTReceiverService: connectionLost: " + arg0.getMessage() + " :" + arg0.toString());
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken arg0) {
        try {
			System.out.println("[LOG] MQTTReceiverService: Delivery completed.\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMsg) {
        try {
            System.out.println("topic :" + topic + " message :" + mqttMsg.toString());
            MessageHandler msgHandler = new MessageHandler(mqttMsg.toString(), this.garageSize, vehicleRepository);
            VehicleMsg vehicleData = mapper.readValue(mqttMsg.toString(), VehicleMsg.class);
            VehicleAcceptanceMsg vehicleAcceptanceMsg = new VehicleAcceptanceMsg();
            vehicleAcceptanceMsg.setLPN(vehicleData.getLPN());
            boolean isAccepted;
            switch (topic) {
                case "ARRIVES":
                    isAccepted = msgHandler.vehicleArrives();
                    vehicleAcceptanceMsg.setIsAccepted(isAccepted);
                    String jsonObject = mapper.writeValueAsString(vehicleAcceptanceMsg);
                    mqttSenderService.publish("ACCEPTANCE", jsonObject);
                    break;
                case "LEAVE":
                    isAccepted = msgHandler.vehicleLeaves();
                    vehicleAcceptanceMsg.setIsAccepted(isAccepted);
                    String jsonObject2 = mapper.writeValueAsString(vehicleAcceptanceMsg);
                    mqttSenderService.publish("EXIT", jsonObject2);
                    break;
                case "PAYMENT:":
                default:
                    System.out.println("[LOG] MQTTReceiverService: Not supported operation for topic: "
                            + topic + ".\n");
            }
            List<Vehicle> list = msgHandler.listGarage();
            String stringList = mapper.writeValueAsString(list);
            System.out.println(stringList);
            mqttSenderService.publish("LIST", stringList);
            System.out.println("[LOG] MQTTReceiverService: List published.\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void publish(String topic, String content) {
        // NO OP - this is just a receiver client, this method left intentionally blank.
    }

    public void startMqtt() {

        MemoryPersistence persistence = new MemoryPersistence();
        try {
            client = new MqttClient(mqttUrl, MqttClient.generateClientId(),
                    persistence);
            // tcp://iot.eclipse.org:1883
        } catch (MqttException e1) {
            e1.printStackTrace();
        }

        MqttConnectOptions connectOptions = new MqttConnectOptions();
        connectOptions.setCleanSession(true);
        connectOptions.setMaxInflight(3000);
        connectOptions.setAutomaticReconnect(true);

        client.setCallback(this);
        mqttSenderService.startMqtt();

        try {
            IMqttToken mqttConnectionToken = client.connectWithResult(connectOptions);

            System.out.println("[LOG] MQTTReceiverService: Connection Status :" + mqttConnectionToken.isComplete());

            client.subscribe(this.topicsToSubscribe);
            MessageHandler msgHandler = new MessageHandler(this.vehicleRepository);
            List<Vehicle> list = msgHandler.listGarage();
            String stringList = mapper.writeValueAsString(list);
            System.out.println("[LOG] MQTTReceiverService:" + stringList);
            mqttSenderService.publish("LIST", stringList);
            System.out.println("[LOG] MQTTReceiverService: List published.\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
