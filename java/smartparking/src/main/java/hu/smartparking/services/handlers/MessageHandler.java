package hu.smartparking.services.handlers;

import hu.smartparking.entities.Vehicle;
import hu.smartparking.repositories.VehicleRepository;
import hu.smartparking.utility.VehicleMsg;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MessageHandler {

    private String message;
    private int garageSize;
    private VehicleRepository vehicleRepository;

    public MessageHandler(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    public MessageHandler(String message, int garageSize,VehicleRepository vehicleRepository) {
        this.message = message;
        this.garageSize = garageSize;
        this.vehicleRepository = vehicleRepository;
    }

    public boolean vehicleArrives() throws JsonProcessingException {
        System.out.println("[LOG] MessageHandler: Registering new vehicle to database...\n");
        ObjectMapper objectMapper = new ObjectMapper();
        VehicleMsg data = objectMapper.readValue(this.message, VehicleMsg.class);

        Optional<Vehicle> oVehicle = vehicleRepository.findByLPNAndLeaveTimeIsNull(data.getLPN());

        if (oVehicle.isPresent()) {
            System.out.println("[LOG] MessageHandler: Vehicle already in the garage! Vehicle declined.\n");
            return false;
        } else if (!checkGarageIsFull()) {
            Vehicle vehicle = new Vehicle();
            vehicle.setEntryTime(getCurrentUTCTimestamp());
            vehicle.setLPN(data.getLPN());
            vehicle.setLeaveTime(null);
            this.vehicleRepository.save(vehicle);
            System.out.println("[LOG] MessageHandler: Vehicle accepted to the garage.\n");
            System.out.println("[LOG] MessageHandler: New vehicle's data: " + vehicle.toString() + "\n");
            return true;
        } else {
            System.out.println("[LOG] MessageHandler: No more space in the garage! Vehicle declined.\n");
            return false;
        }
    }

    public boolean vehicleLeaves() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        VehicleMsg data = objectMapper.readValue(this.message, VehicleMsg.class);

        Optional<Vehicle> oVehicle = vehicleRepository.findByLPNAndLeaveTimeIsNull(data.getLPN());
        System.out.println("[LOG] MessageHandler: Vehicle " + data.getLPN() + " leaving the garage...\n");
        if (oVehicle.isEmpty()) {
            System.out.println("[LOG] MessageHandler: Vehicle with LPN " + data.getLPN() + " not found in garage. " +
                    "Exiting with no operation.\n");
            return false;
        } else {
            Vehicle vehicleUp = oVehicle.get();
            vehicleUp.setLeaveTime(getCurrentUTCTimestamp());
            this.vehicleRepository.save(vehicleUp);
            System.out.println("[LOG] MessageHandler: Vehicle " + data.getLPN() + " left the garage at " +
                    vehicleUp.getLeaveTime() + "\n");
            return true;
        }
    }

    public List<Vehicle> listGarage() {
        System.out.println("[LOG] MessageHandler: Listing vehicles from database...\n");
        List<Vehicle> vehicleList = new ArrayList<>();
        Iterable<Vehicle> vehicles = this.vehicleRepository.findAll();
        for (Vehicle vehicle : vehicles) {
            vehicleList.add(vehicle);
        }
        return vehicleList;
    }

    private boolean checkGarageIsFull() {
        Iterable<Vehicle> vehicles = this.vehicleRepository.findAll();
        int noOfVehiclesInGarage = 0;
        for (Vehicle v : vehicles) {
            if (v.getLeaveTime() == null) {
                noOfVehiclesInGarage ++;
            }
        }
        return noOfVehiclesInGarage >= garageSize;
    }

    private String getCurrentUTCTimestamp() {
        return java.time.Clock.systemUTC().instant().toString(); //2017-01-23T18:35:23.669Z
    }
}
