package hu.smartparking.services;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MQTTSenderService implements MqttCallback {

    private MqttClient client = null;
    private final String mqttUrl;

    @Autowired
    MQTTSenderService(@Value("${mqtt.url}") String mqttUrl) {
        this.mqttUrl = mqttUrl;
        System.out.println(mqttUrl);
    }

    @Override
    public void connectionLost(Throwable arg0) {
        System.out.println("[LOG] MQTTSenderService: connectionLost: " + arg0.getMessage() + " :" + arg0.toString());
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) {
        // NO OP - this is just a sender client, this method left intentionally blank.
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken arg0) {
        try {
            System.out.println("[LOG] MQTTSenderService: Delivery completed.\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void publish(String topic, String content) {
        try {
            if (client.isConnected()) {
                System.out.println("[LOG] MQTTSenderService: Connection Status :" + client.isConnected());
            }
            boolean retained = topic.equals("LIST");
            client.publish(topic, content.getBytes(), 2, retained); //qos 0/1/2
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startMqtt() {

        MemoryPersistence persistence = new MemoryPersistence();
        try {
            client = new MqttClient(mqttUrl, MqttClient.generateClientId(),
                    persistence);
            // tcp://iot.eclipse.org:1883
        } catch (MqttException e1) {
            e1.printStackTrace();
        }

        MqttConnectOptions connectOptions = new MqttConnectOptions();
        connectOptions.setCleanSession(true);
        connectOptions.setMaxInflight(3000);
        connectOptions.setAutomaticReconnect(true);

        client.setCallback(this);

        try {
            IMqttToken mqttConnectionToken = client.connectWithResult(connectOptions);

            System.out.println("[LOG] MQTTSenderService: Connection Status: " + mqttConnectionToken.isComplete());

        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}
