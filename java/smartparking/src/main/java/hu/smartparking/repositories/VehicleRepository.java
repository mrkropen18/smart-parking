package hu.smartparking.repositories;

import hu.smartparking.entities.Vehicle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VehicleRepository extends CrudRepository<Vehicle, Long> {
    Optional<Vehicle> findByLPNAndLeaveTimeIsNull(String LPN); // find not exited car with LPN
}
