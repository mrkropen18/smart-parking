#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <errno.h>
#include <signal.h>
#include <sched.h>
#include <time.h>
#include <mosquitto.h>

#include "network.h"
#include "utils.h"

const int m_length = 17;
const int max_cars = 1024;

struct mosquitto * mosq_arrives;
struct mosquitto * mosq_leaves;
struct mosquitto * mosq_exit;

double startTime = 0.0;
char buf_plate[17];
int cars[1024];
char plates[1024][8];
int car_number;



void on_connect(struct mosquitto *mosq, void *obj, int rc) {
	if(rc) {
		printf("Error with result code: %d\n", rc);
		exit(-1);
	}
	mosquitto_subscribe(mosq, NULL,  exit_topic, 0);
}

void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg) {
	printf("Open the gate for %s:\n",  (char *) msg->payload);
    char* plate = (char *) msg->payload;
    /*for(int i=0; i<car_number;++i){
        if(strcmp(plate,plates[i])){
            //send signal
            
            break;
        }
    }
    */
}

void handler_timer(int signalnumber, siginfo_t *si, void *uc){
    printf("Ellapsed time for %d: %f %s\n", getpid(), getElapsedTime(startTime),  buf_plate);
    mosquitto_publish(mosq_leaves, NULL, leave_topic, m_length, buf_plate, 0, false);

    exit(0);
}


int main()
{

   srand(time(0));
   mosq_arrives = mosquitto_new("ARRIVES", true, NULL);
   mosq_leaves = mosquitto_new("LEAVES", true, NULL);
   mosq_exit = mosquitto_new("EXIT", true, NULL);

   double next_arrivel = inter_arrival_time();
   pid_t child = 1; 

   mqtt_pub_connect(mosq_arrives, ADDRESS, PORT);
   mqtt_pub_connect(mosq_leaves, ADDRESS, PORT);
   mqtt_sub_connect(mosq_exit, ADDRESS, PORT, on_connect, on_message);

   //start the simulation of the car arrivels
   car_number = 0; 
   while(child){

        mosquitto_loop_start(mosq_exit);
	    mosquitto_loop_stop(mosq_exit, true);
        
        printf("Next car will come in %f seconds\n", next_arrivel);
        sleep(next_arrivel);
        printf("Car Enters\n");
        generateLicencePlate(buf_plate);

        for(int i = 0; i<7; ++i){
            plates[car_number][i] = buf_plate[i+8];
        }

        mosquitto_publish(mosq_arrives, NULL, "ARRIVES", m_length, buf_plate, 0, false);

        next_arrivel = inter_arrival_time();
        child = fork(); 
        
   }
   if (child < 0)
   {
      perror("The fork calling was not succesful\n");
      exit(1);
   }

    cars[car_number] = getpid();
    printf("child process %d has plate %s \n", cars[car_number], plates[car_number]);

    car_number+=1;
            
     //SET RR SCHEDULER FOR THIS PROCESS
    struct sched_param schedpar;
    schedpar.sched_priority = 12;
    
    //Requires root access
    if (sched_setscheduler(getpid(), SCHED_RR, &schedpar))
    {
        perror("Failed to set scheduler");
        //exit(1);
    }

    //CREATE TIMER WITH RT SIGNAL
    timer_t timerID;

    struct sigevent sigev;
    sigev.sigev_notify = SIGEV_SIGNAL;
    sigev.sigev_signo = SIGRTMIN + 4;
    sigev.sigev_value.sival_ptr = &timerID; //Passing the timer's ID for the sigactionHandler
    
    //1. parameter: The timer will use this clock
    //2. parameter: Raised sigevent on expiration (NULL means SIGALRM)
    //3. parameter: The generated timer's ID
    if (timer_create(CLOCK_REALTIME, &sigev, &timerID))
    {
        perror("Failed to create Timer");
    //Register signal handler
        exit(1);
    }

    struct sigaction sigact;
    sigemptyset(&sigact.sa_mask); //no blocked signals only the one, which arrives
    sigact.sa_sigaction = handler_timer;
    sigact.sa_flags = SA_SIGINFO;
    sigaction(SIGRTMIN + 4, &sigact, NULL); //an alarm signal is set

    double scheduler_time = stay_time();
    printf("child process %d will stay %f sec \n", getpid(), scheduler_time );


    //set the timer according to stay time
    struct itimerspec timer;
    timer.it_interval.tv_sec = scheduler_time  ;       //it will be repeated after 3 seconds
    timer.it_interval.tv_nsec = 0;      //nsec - nanoseconds - 10^(-9) seconds
    timer.it_value.tv_sec = scheduler_time;          //remaining time till expiration
    timer.it_value.tv_nsec = 0;
    
    startTime = getTime();
    //ARM THE TIMER
    //1. parameter: timer to arm
    //2. parameter: 0 - relative timer, TIMER_ABSTIME - absolute timer
    //3. parameter: expiration and interval settings to be used
    //4. parameter: previous timer settings (if needed)
    timer_settime(timerID, 0, &timer, NULL);

    //wait for expire
    struct itimerspec expires;
    int remaining_time = 1;
    while (remaining_time)
    {
        timer_gettime(timerID, &expires); //reads the remaining time
        remaining_time = expires.it_value.tv_sec;

    }
    sleep(1);

    return 0;
}