#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>


//number of cars entering the parking lot in an hour
const double INCIDENCE_RATE = 15;
//avg time spent in parking place
const double MI = 5; 
//deviation
const double SIGMA = 5;

double rand_gen() {
   // return a uniformly distributed random value
   return ( (double)(rand()) + 1. )/( (double)(RAND_MAX) + 1. );
}

// simulate inter-arrival time
// number of occurrence of events is given by the Poisson process, with incidence rate lambda
// the inter-arrivel time is derived by using Exponential distribution
double inter_arrival_time(){

    return -log(1.0 - rand_gen())/INCIDENCE_RATE * 60;

}

//normally distributed numbers based on Box-Mulller transform
double stay_time(){

   double v1=rand_gen();
   double v2=rand_gen();
   return cos(2*3.14*v2)*sqrt(-2.*log(v1)) * SIGMA + MI;
}

double getTime()
{
    struct timespec now;
    clock_gettime(CLOCK_REALTIME, &now);
    return now.tv_sec + now.tv_nsec * 1e-9;
}

double getElapsedTime(double startTime)
{
    return getTime() - startTime;
}

void getUTCTime(char* buf){
    time_t now = time(&now);
    struct tm *ptm = gmtime(&now);

    strftime(buf, 256, "%Y.%m.%d %T UTC", ptm);
}

void generateLicencePlate(char *randomString){
    static char charset[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";        
    static char numberset[] = "0123456789";        

    randomString[0] = '{';
    randomString[1] = '"';

    randomString[2] = 'l';
    randomString[3] = 'p';
    randomString[4] = 'n';

    randomString[5] = '"';

    randomString[6] = ':';
    randomString[7] = '"';


    for (int n = 8;n < 11;n++) {            
        int key = rand() % (int)(sizeof(charset) -1);
        randomString[n] = charset[key];
    }

    randomString[11] = '-';


    for (int n = 12;n < 15;n++) {            
        int key = rand() % (int)(sizeof(numberset) -1);
        randomString[n] =  numberset[key];
    }
    
    randomString[15] = '"';
    randomString[16] = '}';

    randomString[17] = '\0';

        
}