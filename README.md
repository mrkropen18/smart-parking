# Smart Parking

## run with auto exit after one container exits: 

```shell
docker-compose up --build --abort-on-container-exit
```

## run containers (need to stop by hand the non-stopping ones): 

```shell
docker-compose up --build
```

# 